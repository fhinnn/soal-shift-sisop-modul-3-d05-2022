# Penyelesaian Soal Shift 3 Sistem Operasi

## Kelompok D05

| Nama | NRP |
| ------ | ------ |
| DHAFIN ALMAS NUSANTARA | 5025201064 |
| NEISA HIBATILLAH ALIF | 5025201170 |
| FEBERLIZER EDNAR WILLIAM GULTOM|05111940007004|


## ---SOAL 1---
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat `base64`. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan `base 64`. Agar lebih cepat, Ia sarankan untuk menggunakan thread.


## Soal a
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip `quote.zip` dan music untuk file zip `music.zip`. Unzip ini dilakukan dengan bersamaan menggunakan thread.


Code download file :
```
void* download_file(void *arg) {
    char *data = (char *) arg;
    char *filename, *link;

    filename = strtok(data, "|");
    link = strtok(NULL, "|");

    char *argv[] = {"wget", "-O", filename, link, "-q", NULL};

    pid_t child = fork();
    int status;

    if (child < 0) {
        puts("ERROR FORKING THREAD DOWNLOAD");
    } else if (child == 0) {
        execv("/usr/bin/wget", argv);
    } else {
        waitpid(child, &status, 0);
    }
    pthread_exit(NULL);
}
```
Code untuk ekstrak file download :
```
void* extract_zip(void* arg) {
    char *foldername = (char*) arg;

    pid_t child = fork();
    int status;
    if (child < 0) {
        puts("ERROR FORKING THREAD EXTRACT");
    } else if (child == 0) {
        char *argv[] = {"unzip", "-q", foldername, "-d", foldername, NULL};
        execv("/usr/bin/unzip", argv);
    } else {
        waitpid(child, &status, 0);
    }
    pthread_exit(NULL);
}
```
Penjelasan: 
- membuat fungsi yang berisi perintah download file dan ekstrak file yang nanti akan dipanggil dalam funngsi `main ()`
dengan menggunakan `wait` untuk menunggu fungsi sebelumnya selesai melakukan tugasnya.

## Soal b
Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
```
void* decode_txt(void *arg) {
    char foldername[100], output_filename[100], cwd[256];
    getcwd(cwd, sizeof(cwd));
    strcpy(foldername, cwd);
    strcat(foldername, "/");
    strcat(foldername, (char *) arg);

    strcpy(output_filename, foldername);
    strcat(output_filename, "/");
    strcat(output_filename, (char *) arg);
    strcat(output_filename, ".txt");

    int status;
    DIR *dp;
    struct dirent *ep;
    FILE *fp_input, *fp_output;
    char line[128], input_filename[100];
    unsigned char *output_line;

    fp_output = fopen(output_filename, "w");

    dp = opendir(foldername);
    if (dp != NULL) {
        while((ep = readdir(dp))) {
            if (strcmp(ep->d_name, ".") && strcmp(ep->d_name, "..")) {
                strcpy(input_filename, foldername);
                strcat(input_filename, "/");
                strcat(input_filename, ep->d_name);

                if (!strcmp(input_filename, output_filename)) continue;

                fp_input = fopen(input_filename, "r");
                if (fp_input != NULL) {
                    while (fgets(line, sizeof(line), fp_input) != NULL) {
                        output_line = b64_decode(line, strlen(line));
                        fprintf(fp_output, "%s\n", output_line);
                    }
                }
                fclose(fp_input);
            }
        }
    }
    fclose(fp_output);
    pthread_exit(NULL);
}
```
Penjelasan :
Membuat fungsi untuk mengambil semua yang ada dalam dokumne dengan mencopy dengan `strcpy` dan menggabungkannya dengan `strcat` yang nanti akan dimasukkan dalam file txt yang telah disiapkan dan dipisahkan dengan newline.

## Soal c
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
```
void* move_output_file(void * arg) {
    char foldername[100], cwd[256], input_file[100], output_file[100];
    getcwd(cwd, sizeof(cwd));
    strcpy(foldername, cwd);
    strcat(foldername, "/");
    strcat(foldername, (char *) arg);

    strcpy(input_file, foldername);
    strcat(input_file, "/");
    strcat(input_file, (char *) arg);
    strcat(input_file, ".txt");

    strcpy(output_file, cwd);
    strcat(output_file, "/hasil/");
    strcat(output_file, (char *) arg);
    strcat(output_file, ".txt");

    pid_t child = fork();
    int status;
    if (child < 0) {
        puts("ERROR FORKING CREATE FOLDER HASIL");
    } else if (child == 0) {
        char *argv[] = {"mv", input_file, output_file, NULL};
        execv("/usr/bin/mv", argv);
    } else {
        waitpid(child, &status, 0);
    }

    pthread_exit(NULL);
}
```
Penjelasan :
Membuat fungsi yang dibuat untuk mencopy semua output yang ada untuk dipindahkan kedalam folder baru dengan mencopy semua string dengan `strcpy` dan menggabungkan string tersebut dengan `strcat` dengan `mv` untuk memindah file txt ke folder bernama hasil.

## Soal d
Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest [Nama user]'. (contoh password : mihinomenestnovak)
```
child = fork();
    if (child < 0) {
        puts("ERROR FORKING ZIP FOLDER HASIL");
    } else if (child == 0) {
        char *argv[] = {"zip", "--password", "mihinomenestdhafin", "-r", "hasil.zip", "hasil", NULL};
        execv("/usr/bin/zip", argv);
    } else {
        waitpid(child, &status, 0);
    }
```
Penjelasan :
Membuat fork untuk meng-zip folder hasil dengan password yang telah ditentukan.

## Soal e
Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
```
void* unzip (void * arg) {
    pid_t child = fork();
    int status;
    if (child < 0) {
        puts("ERROR FORKING CREATE FOLDER HASIL");
    } else if (child == 0) {
        char *argv[] = {"unzip", "-P", "mihinomenestfajar", "hasil.zip", NULL};
        execv("/usr/bin/unzip", argv);
    } else {
        waitpid(child, &status, 0);
    }
    pthread_exit(NULL);
}
```
```
void* create_file_no (void * arg) {
    FILE *fp;
    fp = fopen("no.txt", "w");
    fputs("No", fp);
    fclose(fp);
    pthread_exit(NULL);
}
```
Penjelasan :
Membuat fungsi dengan mengunzip kembali folder hasil dan menambahkan kata No dengan fungsi baru dan akan dipanggil dalam thread yang sama agar dapat dijalankan diwaktu yang bersamaan.

Kendala yang dialami:
- masih bingung dalam pengguanaan thread dan syntax syntax bahasa c seperti `strcat`,`strcpy`, dll.


## ---SOAL 2---
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

## Soal a
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file `users.txt` dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di `users.txt` yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
● Username unique (tidak boleh ada user yang memiliki username yang sama)
● Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil

Code `Server.c`
Fungsi untuk register:
```
void regist(){
    while(true){
        char c_username[50] = {0};
        char c_password[50] = {0};

        read(new_socket, c_username, 1024);

        FILE *file;
        file = fopen("users.txt", "a+");
        char line[100];

        bool exist = false;
        while(fgets(line, sizeof(line), file) != NULL){
            char *username = strtok(line, ":");
            char *password = strtok(NULL, ":");

            if(strcmp(username, c_username) == 0){
                printf("%s", password);
                exist = true;
            }
        }

        if(exist){
            send(new_socket, "exist", strlen("exist"), 0);
            continue;
        } else{
            send(new_socket, "regist", strlen("regist"), 0);
            read(new_socket, c_password, 1024);

            fprintf(file, "%s:%s\n", c_username, c_password);
            fclose(file);

            printf("User %s telah terdaftar", c_username);
            break;
        }
    }
}
```

Fungsi untuk login:
```
void login(char user[], bool *loggedin){
    char c_username[50] = {0};
    char c_password[50] = {0};

    read(new_socket, c_username, 1024);
    read(new_socket, c_password, 1024);

    FILE *users_file;
    users_file = fopen("users.txt", "a+");
    char line[100];
    bool success = false;

    while(fgets(line, sizeof(line), users_file) != NULL){
        char *username = strtok(line, ":");
        char *password = strtok(NULL, ":");

        if(strncmp(username, c_username, strlen(c_username)) == 0 && strncmp(password, c_password, strlen(c_password)) == 0) success = true;
    }

    fclose(users_file);

    if(success){
        send(new_socket, c_username, sizeof(c_username), 0);
        printf("User %s telah masuk\n", c_username);
        strcpy(user, c_username);
        *loggedin = true;
    }
    else send(new_socket, "failed", sizeof("failed"), 0);
}
```

Code `Client.c`
Fungsi untuk register:
```
void regist(){
    char username[50] = {0};
    char password[50] = {0};
    bool success = false;

    while(!success){
        printf("Username: ");
        scanf("%s", username);
        printf("Password: ");
        scanf("%s", password);

        if(!check(password)){
            printf("Format password belum valid!\n");
            continue;
        }

        send_str(username);
        read_str(buffer);

        if(strcmp(buffer, "exist") == 0){
            printf("Username telah digunakan.\n");
            continue;
        }

        send_str(password);

        printf("Register berhasil!\n");
        success = true;
    }

}
```

Fungsi untuk login:
```
void login(char user[], bool *loggedin){
    char username[50] = {0};
    char password[50] = {0};
    bool success = false;

    while(!success){
        printf("Username: ");
        scanf("%s", username);
        printf("Password: ");
        scanf("%s", password);

        if(!check(password)){
            printf("Format password belum valid!\n");
            continue;
        }

        send_str(username);
        send_str(password);
        read_str(buffer);

        if(strcmp(buffer, "failed") == 0){
            printf("Login gagal.\n");
        } else{
            printf("Login berhasil!\n");
            strcpy(user, buffer);
            *loggedin = true;
        }
        success = true;
    }
}
```

Fungsi untuk mengecek kriteria password:
```
bool check(char *password){
    const char *p = password;
    char c;

    int chr = 0;
    int upper = 0;
    int lower = 0;
    int digit = 0;

    while(*p){
        c = *p++;
        chr++;

        if(isupper(c)) upper++;
        else if(islower(c)) lower++;
        else if(isdigit(c)) digit++;
        else continue;
    }

    return chr > 5 && upper && lower && digit;
}
```

Mengecek kriteria username:
```
...
send_str(username);
        read_str(buffer);

        if(strcmp(buffer, "exist") == 0){
            printf("Username telah digunakan.\n");
            continue;
        }
...
```

## Soal b
Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan `\t`. File otomatis dibuat saat server dijalankan.

Code:
```
...
FILE *database;
database = fopen("problems.tsv", "a+");
fclose(database);
...
```

Penjelasan:
- `fopen` digunakan untuk membuka file.

## Soal c
Client yang telah login, dapat memasukkan command yaitu `add` yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
● Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
● Path file `description.txt` pada client (file ini berisi deskripsi atau penjelasan problem)
● Path file `input.txt` pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
● Path file `output.txt` pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)

## Soal d
Client yang telah login, dapat memasukkan command `see` yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya (author merupakan username client yang menambahkan problem tersebut). 

## Soal e
Client yang telah login, dapat memasukkan command `download <judul-problem>` yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu `<judul-problem>`. Kedua file tersebut akan disimpan ke folder dengan nama `<judul-problem>` di client.

## Soal f
Client yang telah login, dapat memasukkan command `submit <judul-problem> <path-file-output.txt>`. Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file `output.txt` nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file `output.txt` yang telah dikirimkan oleh client dan `output.txt` yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan `AC` dan jika tidak maka server akan mengeluarkan pesan `WA`.

## Soal g
Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.


## ---SOAL 3---
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

## Soal a dan b
Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder `/home/[user]/shift3/`. Kemudian working directory program akan berada pada folder `/home/[user]/shift3/hartakarun/`. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif.

Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder `Unknown`. Jika file hidden, masuk folder `Hidden`.

Code
Listing file:
```
void* list(char *base_path){
  char path[1000];
  struct dirent *dp;
  DIR *dir = opendir(base_path);

  if(!dir) return;

  while((dp = readdir(dir)) != NULL) {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
        strcpy(path, base_path);
        strcat(path, "/");
        strcat(path, dp->d_name);
    
        char ext[10000];
        snprintf(ext, sizeof ext, "%s", dp->d_name);
        
        if(ext[0] == '.') {
            mkdir("Hidden", 0777);

            char moves[10000];
            snprintf(moves, sizeof moves, "Hidden/%s", dp->d_name);
            rename(path, moves);
        }else {
            char loc[10000];
            strcpy(loc, path);
            snprintf(info[files], sizeof loc, "%s", loc);
            files++;
        }
      list(path);
    }
  }
  closedir(dir);
}
```

Mendapatkan nama folder atau kategori file:
```
char* folder_name(char *path_to_file){
  char *folder_name;
  char *str = folder;

  char *ext;
  ext = strrchr(str, '/'); 

  if(ext != NULL) {
    if (ext[1] == '.') return "Hidden";
  }

  folder_name = strtok(str, ".");
  folder_name = strtok(NULL, "");

  if(folder_name == NULL) return "Unknown";

  for(int i=0; folder_name[i]; i++) folder_name[i] = tolower(folder_name[i]);

  return folder_name;
}
```

Mendapatkan nama file:
```
char* file_name(char *path_to_file){
  char *str = path_to_file;
  char *file_name[10];
  int count = 0;

  char *temp;
  temp = strtok(str, "/");

  while(temp != NULL) {
    file_name[count] = temp;
    temp = strtok(NULL, "/");
    count++;
  }

  return file_name[count - 1];
}
```

Memindahkan file ke dalam folder sesuai kategorinya:
```
void* move(char *file_buffer, char *file_name, char *folder_name){
  char name[1000];

  if (strcmp(folder_name, "Hidden") == 0 || strcmp(folder_name, "Unknown") == 0 ) {
    snprintf(name, sizeof name, "%s/%s", folder_name, file_name);
  }else {
    snprintf(name, sizeof name, "%s/%s.%s", folder_name, file_name, folder_name);
  }

  rename(file_buffer, name);
}
```

## Soal c
Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

Code:
```
...
void* process(void *path_to_file){
  char *folder_name, *file_name, *file_path, file_buffer[10000];
  DIR *dir = opendir(file_path);

  file_path = (char *) path_to_file;

  if(dir == NULL){
    folder_name = folder_name(path_to_file);
    file_name = file_name(path_to_file);

    snprintf(file_buffer, sizeof file_buffer, "%s", file_path);
    mkdir(folder_name, 0777);
    move(file_buffer, file_name, folder_name);
  }

}

int main(){
  list(folder);

  for(int i=0; i<files; i++) pthread_create(&(tid[i]), NULL, process, (char*) info[i]);
  for(int i=0; i<=files; i++) pthread_join(tid[i], NULL);
  
  exit(EXIT_SUCCESS);
}
```

## Soal d dan e
Untuk mengirimkan file ke Cocoyasi Village, Nami menggunakan program client-server. Saat program client dijalankan, maka folder `/home/[user]/shift3/hartakarun/` akan di-zip terlebih dahulu dengan nama `hartakarun.zip` ke working directory dari program client.

Client dapat mengirimkan file `hartakarun.zip` ke server dengan mengirimkan command `send hartakarun.zip` ke server.

Code
Zip folder:
```
void* zip() {
  pid_t child_id;
  int status1;

  child_id = fork();

  if(child_id == 0) {
    char *argv[] = {"zip", "-q", "-r", "./hartakarun.zip", "/home/neisa/shift3/hartakarun", NULL};
    execv("/usr/bin/zip", argv);
  }else {
    while (wait(&status1) > 0);
  }
}
```

Membaca argumen `send`:
```
...
if(argc > 1) {
    if(strcmp(argv[1], "send") == 0) {
      if (argc > 2) {
        file_name = argv[2];
      }else {
        exit(EXIT_FAILURE);
      }
    }else {
      exit(EXIT_FAILURE);
    }
  }
...
```

Kendala yang dialami:
- Masih belum terbiasa dengan berbagai fungsi file handling pada C.
