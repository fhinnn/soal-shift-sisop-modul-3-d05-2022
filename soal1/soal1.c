#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<dirent.h>
#include "deps/b64/b64.h"

pthread_t dtid[2];
pthread_t etid[2];
pthread_t dectid[2];
pthread_t mvtid[2];

void* download_file(void *arg);
void* extract_zip(void* arg);
void* decode_txt(void *arg);
void* move_output_file(void * arg);
void* unzip (void * arg);
void* create_file_no (void * arg);

int main() {
    char data[2][] = {
            "music.zip|https://drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1",
            "quote.zip|https://drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt"
    };

    char name[2][] = {"music", "quote"};

    int len = sizeof(dtid) / sizeof(dtid[0]);
    int err;

    for (int i = 0; i < len; ++i) {
        err = pthread_create(&(dtid[i]), NULL, &download_file, (void *) data[i]);

        if (err != 0) {
            printf("%d - ERROR\n", err);
        }
    }

    pthread_join(dtid[0], NULL);
    pthread_join(dtid[1], NULL);

    for (int i = 0; i < len; ++i) {
        err = pthread_create(&(etid[i]), NULL, &extract_zip, (void *) name[i]);

        if (err != 0) {
            printf("%d - ERROR\n", err);
        }
    }

    pthread_join(etid[0], NULL);
    pthread_join(etid[1], NULL);

    for (int i = 0; i < len; ++i) {
        err = pthread_create(&(dectid[i]), NULL, &decode_txt, (void *) name[i]);

        if (err != 0) {
            printf("%d - ERROR\n", err);
        }
    }

    pthread_join(dectid[0], NULL);
    pthread_join(dectid[1], NULL);

    pid_t child = fork();
    int status;
    if (child < 0) {
        puts("ERROR");
    } else if (child == 0) {
        char *argv[] = {"mkdir", "-p", "hasil", NULL};
        execv("/usr/bin/mkdir", argv);
    } else {
        waitpid(child, &status, 0);
    }

    for (int i = 0; i < len; ++i) {
        err = pthread_create(&(mvtid[i]), NULL, &move_output_file, (void *) name[i]);

        if (err != 0) {
            printf("%d - ERROR\n", err);
        }
    }

    pthread_join(mvtid[0], NULL);
    pthread_join(mvtid[1], NULL);

    child = fork();
    if (child < 0) {
        puts("ERROR");
    } else if (child == 0) {
        char *argv[] = {"zip", "-P", "mihinomenestdhafin", "-r", "hasil.zip", "hasil", NULL};
        execv("/usr/bin/zip", argv);
    } else {
        waitpid(child, &status, 0);
    }

    child = fork();
    if (child < 0) {
        puts("ERROR REMOVE FOLDER HASIL");
    } else if (child == 0) {
        char *argv[] = {"rm", "-r", "hasil", NULL};
        execv("/usr/bin/rm", argv);
    } else {
        waitpid(child, &status, 0);
    }

    pthread_t unzip_tid;
    pthread_t makefile_tid;

    err = pthread_create(&unzip_tid, NULL, &unzip, NULL);
    if (err != 0) {
        printf("%d - ERROR\n", err);
    }

    err = pthread_create(&makefile_tid, NULL, &create_file_no, NULL);
    if (err != 0) {
        printf("%d - ERROR\n", err);
    }

    pthread_join(unzip_tid, NULL);
    pthread_join(makefile_tid, NULL);

    child = fork();
    if (child < 0) {
        puts("ERROR");
    } else if (child == 0) {
        char *argv[] = {"zip", "-P", "mihinomenestdhafin", "-r", "hasil.zip", "hasil", "no.txt", NULL};
        execv("/usr/bin/zip", argv);
    } else {
        waitpid(child, &status, 0);
    }

    child = fork();
    if (child < 0) {
        puts("ERROR");
    } else if (child == 0) {
        char *argv[] = {"rm", "-r", "hasil", NULL};
        execv("/usr/bin/rm", argv);
    } else {
        waitpid(child, &status, 0);
    }

    child = fork();
    if (child < 0) {
        puts("ERROR");
    } else if (child == 0) {
        char *argv[] = {"rm", "no.txt", NULL};
        execv("/usr/bin/rm", argv);
    } else {
        waitpid(child, &status, 0);
    }
    return 0;
}

void* download_file(void *arg) {
    char *data = (char *) arg;
    char *filename, *link;

    filename = strtok(data, "|");
    link = strtok(NULL, "|");

    char *argv[] = {"wget", "-O", filename, link, "-q", NULL};

    pid_t child = fork();
    int status;

    if (child < 0) {
        puts("ERROR");
    } else if (child == 0) {
        execv("/usr/bin/wget", argv);
    } else {
        waitpid(child, &status, 0);
    }
    pthread_exit(NULL);
}

void* extract_zip(void* arg) {
    char *foldername = (char*) arg;

    pid_t child = fork();
    int status;
    if (child < 0) {
        puts("ERROR");
    } else if (child == 0) {
        char *argv[] = {"unzip", "-q", foldername, "-d", foldername, NULL};
        execv("/usr/bin/unzip", argv);
    } else {
        waitpid(child, &status, 0);
    }
    pthread_exit(NULL);
}

void* decode_txt(void *arg) {
    char foldername[100], output_filename[100], cwd[256];
    getcwd(cwd, sizeof(cwd));
    strcpy(foldername, cwd);
    strcat(foldername, "/");
    strcat(foldername, (char *) arg);

    strcpy(output_filename, foldername);
    strcat(output_filename, "/");
    strcat(output_filename, (char *) arg);
    strcat(output_filename, ".txt");

    int status;
    DIR *dp;
    struct dirent *ep;
    FILE *fp_input, *fp_output;
    char line[128], input_filename[100];
    unsigned char *output_line;

    fp_output = fopen(output_filename, "w");

    dp = opendir(foldername);
    if (dp != NULL) {
        while((ep = readdir(dp))) {
            if (strcmp(ep->d_name, ".") && strcmp(ep->d_name, "..")) {
                strcpy(input_filename, foldername);
                strcat(input_filename, "/");
                strcat(input_filename, ep->d_name);

                if (!strcmp(input_filename, output_filename)) continue;

                fp_input = fopen(input_filename, "r");
                if (fp_input != NULL) {
                    while (fgets(line, sizeof(line), fp_input) != NULL) {
                        output_line = b64_decode(line, strlen(line));
                        fprintf(fp_output, "%s\n", output_line);
                    }
                }
                fclose(fp_input);
            }
        }
    }
    fclose(fp_output);
    pthread_exit(NULL);
}

void* move_output_file(void * arg) {
    char foldername[100], cwd[256], input_file[100], output_file[100];
    getcwd(cwd, sizeof(cwd));
    strcpy(foldername, cwd);
    strcat(foldername, "/");
    strcat(foldername, (char *) arg);

    strcpy(input_file, foldername);
    strcat(input_file, "/");
    strcat(input_file, (char *) arg);
    strcat(input_file, ".txt");

    strcpy(output_file, cwd);
    strcat(output_file, "/hasil/");
    strcat(output_file, (char *) arg);
    strcat(output_file, ".txt");

    pid_t child = fork();
    int status;
    if (child < 0) {
        puts("ERROR");
    } else if (child == 0) {
        char *argv[] = {"mv", input_file, output_file, NULL};
        execv("/usr/bin/mv", argv);
    } else {
        waitpid(child, &status, 0);
    }

    pthread_exit(NULL);
}

void* unzip (void * arg) {
    pid_t child = fork();
    int status;
    if (child < 0) {
        puts("ERROR");
    } else if (child == 0) {
        char *argv[] = {"unzip", "-P", "mihinomenestdhafin", "hasil.zip", NULL};
        execv("/usr/bin/unzip", argv);
    } else {
        waitpid(child, &status, 0);
    }
    pthread_exit(NULL);
}

void* create_file_no (void * arg) {
    FILE *fp;
    fp = fopen("no.txt", "w");
    fputs("No", fp);
    fclose(fp);
    pthread_exit(NULL);
}

