#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>
#include <errno.h>

int files = 0;
int status = 0;
char info[1000][1000];
pthread_t tid[1000];
char *folder = "/home/neisa/shift3/hartakarun/";

void* list(char *base_path);
char* folder_name(char *path_to_file);
char* file_name(char *path_to_file);
void* move(char *file_buffer, char *file_name, char *folder_name);
void* process(void *path_to_file);

void* list(char *base_path){
  char path[1000];
  struct dirent *dp;
  DIR *dir = opendir(base_path);

  if(!dir) return;

  while((dp = readdir(dir)) != NULL) {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
        strcpy(path, base_path);
        strcat(path, "/");
        strcat(path, dp->d_name);
    
        char ext[10000];
        snprintf(ext, sizeof ext, "%s", dp->d_name);
        
        if(ext[0] == '.') {
            mkdir("Hidden", 0777);

            char moves[10000];
            snprintf(moves, sizeof moves, "Hidden/%s", dp->d_name);
            rename(path, moves);
        }else {
            char loc[10000];
            strcpy(loc, path);
            snprintf(info[files], sizeof loc, "%s", loc);
            files++;
        }
      list(path);
    }
  }
  closedir(dir);
}

char* folder_name(char *path_to_file){
  char *folder_name;
  char *str = folder;

  char *ext;
  ext = strrchr(str, '/'); 

  if(ext != NULL) {
    if (ext[1] == '.') return "Hidden";
  }

  folder_name = strtok(str, ".");
  folder_name = strtok(NULL, "");

  if(folder_name == NULL) return "Unknown";

  for(int i=0; folder_name[i]; i++) folder_name[i] = tolower(folder_name[i]);

  return folder_name;
}

char* file_name(char *path_to_file){
  char *str = path_to_file;
  char *file_name[10];
  int count = 0;

  char *temp;
  temp = strtok(str, "/");

  while(temp != NULL) {
    file_name[count] = temp;
    temp = strtok(NULL, "/");
    count++;
  }

  return file_name[count - 1];
}

void* move(char *file_buffer, char *file_name, char *folder_name){
  char name[1000];

  if (strcmp(folder_name, "Hidden") == 0 || strcmp(folder_name, "Unknown") == 0 ) {
    snprintf(name, sizeof name, "%s/%s", folder_name, file_name);
  }else {
    snprintf(name, sizeof name, "%s/%s.%s", folder_name, file_name, folder_name);
  }

  rename(file_buffer, name);
}

void* process(void *path_to_file){
  char *folder_name, *file_name, *file_path, file_buffer[10000];
  DIR *dir = opendir(file_path);

  file_path = (char *) path_to_file;

  if(dir == NULL){
    folder_name = folder_name(path_to_file);
    file_name = file_name(path_to_file);

    snprintf(file_buffer, sizeof file_buffer, "%s", file_path);
    mkdir(folder_name, 0777);
    move(file_buffer, file_name, folder_name);
  }
}

int main(){
  list(folder);

  for(int i=0; i<files; i++) pthread_create(&(tid[i]), NULL, process, (char*) info[i]);
  for(int i=0; i<=files; i++) pthread_join(tid[i], NULL);
  
  exit(EXIT_SUCCESS);
}
