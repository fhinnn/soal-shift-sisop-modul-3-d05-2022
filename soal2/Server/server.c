#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#define PORT 8080

int socket();
void regist();
void login(char user[], bool *loggedin);

int server_fd, new_socket, valread;
struct sockaddr_in address;
int opt = 1;
int addrlen = sizeof(address);
char client[] = "/home/neisa/shift3/soal2/Client"

int socket(){
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0){
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0){
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0){
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0){
        perror("accept");
        exit(EXIT_FAILURE);
    }
}

void regist(){
    while(true){
        char c_username[50] = {0};
        char c_password[50] = {0};

        read(new_socket, c_username, 1024);

        FILE *file;
        file = fopen("users.txt", "a+");
        char line[100];

        bool exist = false;
        while(fgets(line, sizeof(line), file) != NULL){
            char *username = strtok(line, ":");
            char *password = strtok(NULL, ":");

            if(strcmp(username, c_username) == 0){
                printf("%s", password);
                exist = true;
            }
        }

        if(exist){
            send(new_socket, "exist", strlen("exist"), 0);
            continue;
        } else{
            send(new_socket, "regist", strlen("regist"), 0);
            read(new_socket, c_password, 1024);

            fprintf(file, "%s:%s\n", c_username, c_password);
            fclose(file);

            printf("User %s telah terdaftar", c_username);
            break;
        }
    }
}

void login(char user[], bool *loggedin){
    char c_username[50] = {0};
    char c_password[50] = {0};

    read(new_socket, c_username, 1024);
    read(new_socket, c_password, 1024);

    FILE *users_file;
    users_file = fopen("users.txt", "a+");
    char line[100];
    bool success = false;

    while(fgets(line, sizeof(line), users_file) != NULL){
        char *username = strtok(line, ":");
        char *password = strtok(NULL, ":");

        if(strncmp(username, c_username, strlen(c_username)) == 0 && strncmp(password, c_password, strlen(c_password)) == 0) success = true;
    }

    fclose(users_file);

    if(success){
        send(new_socket, c_username, sizeof(c_username), 0);
        printf("User %s telah masuk\n", c_username);
        strcpy(user, c_username);
        *loggedin = true;
    }
    else send(new_socket, "failed", sizeof("failed"), 0);
}

int main(int argc, char const *argv[]){
    char user[50];
    bool loggedin = false;

    if(socket() < 0) return -1;

    FILE *database;
    database = fopen("problems.tsv", "a+");
    fclose(database);

    while(true){
        char command[30] = {0};
        read(new_socket, command, 1024);

        if(strcmp(command, "register") == 0){
            printf("register\n");
            regist();
        } else if(strcmp(command, "login") == 0){
            printf("login\n"); 
            login(user, &loggedin);
        }
    }

    return 0;
}
