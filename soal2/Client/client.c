#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080
#define HOST "127.0.0.1"

int socket();
void send_str();
void read_str();
bool check();
void regist();
void login(char user[], bool *loggedin);

struct sockaddr_in address;
int sock = 0, valread;
struct sockaddr_in serv_addr;
char buffer[1024] = {0};

int socket(){
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, HOST, &serv_addr.sin_addr) <= 0){
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0){
        printf("\nConnection Failed \n");
        return -1;
    }
}

void send_str(char *str){
    send(sock, str, strlen(str), 0);
}

void read_str(char *str){
    read(sock, str, sizeof(str));
}

bool check(char *password){
    const char *p = password;
    char c;

    int chr = 0;
    int upper = 0;
    int lower = 0;
    int digit = 0;

    while(*p){
        c = *p++;
        chr++;

        if(isupper(c)) upper++;
        else if(islower(c)) lower++;
        else if(isdigit(c)) digit++;
        else continue;
    }

    return chr > 5 && upper && lower && digit;
}

void regist(){
    char username[50] = {0};
    char password[50] = {0};
    bool success = false;

    while(!success){
        printf("Username: ");
        scanf("%s", username);
        printf("Password: ");
        scanf("%s", password);

        if(!check(password)){
            printf("Format password belum valid!\n");
            continue;
        }

        send_str(username);
        read_str(buffer);

        if(strcmp(buffer, "exist") == 0){
            printf("Username telah digunakan.\n");
            continue;
        }

        send_str(password);

        printf("Register berhasil!\n");
        success = true;
    }

}

void login(char user[], bool *loggedin){
    char username[50] = {0};
    char password[50] = {0};
    bool success = false;

    while(!success){
        printf("Username: ");
        scanf("%s", username);
        printf("Password: ");
        scanf("%s", password);

        if(!check(password)){
            printf("Format password belum valid!\n");
            continue;
        }

        send_str(username);
        send_str(password);
        read_str(buffer);

        if(strcmp(buffer, "failed") == 0){
            printf("Login gagal.\n");
        } else{
            printf("Login berhasil!\n");
            strcpy(user, buffer);
            *loggedin = true;
        }
        success = true;
    }
}

int main(int argc, char const *argv[]){
    char user[50];
    bool loggedin = false;

    if(socket() < 0) return -1;

    while(true){
        if(loggedin){
            char command[30];
            printf("command: ");
            scanf("%s", command);
            if (strcmp(command, "add") == 0) send_str("add");

        } else{
            printf("====== Bluemary Online Judge ======\n");
            printf("Menu:\n");
            printf("1. Register\n");
            printf("2. Login\n");

            int input;
            scanf("%d", input);

            if(input == 1){
                send_str("register");
                regist();
            } else if(input == 2){
                send_str("login");
                login(user, &loggedin);
            }
        }
    }

    return 0;
}
